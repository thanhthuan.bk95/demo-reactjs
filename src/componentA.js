'use-strict';

import store from "../main";

import ComponentB from "./componentB";
import React, {PureComponent} from "react";
import GetRandomColor from "./randomColor";

class ComponentA extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            colorA: '#f9f9f9',
            colorB: null,
        };

        this.changeColorB = this.changeColorB.bind(this);
        this.changeColorA = this.changeColorA.bind(this);
    }

    changeColorB() {
        this.setState({
            colorB: GetRandomColor()
        });
    }

    changeColorA() {
        this.setState({
            colorA: GetRandomColor()
        });
    }

    render() {
        return (
            <div style={{height: 500, width: 500, backgroundColor: this.state.colorA, padding: 30}}>
                Component A<br/>
                <button onClick={this.changeColorB}>Button A</button>
                <ComponentB colorB={this.state.colorB} changeColorA={this.changeColorA}/>
            </div>
        );
    }
}

export default ComponentA;