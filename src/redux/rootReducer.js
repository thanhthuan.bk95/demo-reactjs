import {PLUS, MINUS} from "./actionTypes";

const initState = {value: 0, abc: 1};

export default (state = initState, action) => {
    switch (action.type) {
        case PLUS:
            return {value: state.value + 1};
            break;
        case MINUS:
            return {value: state.value - 1};
            break;
        default:
            return state;
    }
}