import React, {Component} from "react";
import ComponentA from "./src/componentA";

'use-strict';

class App extends Component {
    render() {
        return (
            <ComponentA/>
        );
    }
}
export default App;
