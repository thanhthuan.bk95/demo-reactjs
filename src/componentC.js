'use-strict';
import {ACTION_TYPE, actionCreator, MINUS} from "./redux/actionTypes";

import React, {PureComponent} from "react";
import {connect} from "react-redux";
import {minusAction} from "./redux/actions";

class ComponentC extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{height: 200, width: 200, backgroundColor: this.props.colorC, padding: 30}}>
                Component C {this.props.prop1}
                <button onClick={this.props.changeColorA}>Button C</button><br/>
                <button onClick={()=> {this.props.minus()}}>-</button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {prop1: state.value}
}

function mapDispatchToProps(dispatch) {
    return{
        minus: () => {
            dispatch(minusAction(MINUS))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentC);
