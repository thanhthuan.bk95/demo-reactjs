import {PLUS, MINUS} from "./actionTypes";

export const plusAction = () => ({
    type: PLUS,
    payload: null
})

export const minusAction = (type1) => ({
    type: type1,
    payload: null
})
