import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';
import {createStore, applyMiddleware} from "redux";
import rootReducer from "./src/redux/rootReducer";
import thunk from "redux-thunk";
import { Provider} from "react-redux";
import {composeWithDevTools} from 'redux-devtools-extension';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

const show = store.getState();

export default class DemoRedux extends Component {
    render() {
        return (
            <Provider store={store}>
                <App/>
            </Provider>
        );
    }
}

ReactDOM.render(<DemoRedux />, document.getElementById('app'));
