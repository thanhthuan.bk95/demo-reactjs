'use-strict';
import ComponentC from "./componentC";
import React, {PureComponent} from "react";
import GetRandomColor from "./randomColor";
import PropTypes from "prop-types";

class ComponentB extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            colorC: '#999999'
        };

        this.changeColorC = this.changeColorC.bind(this);
        this.plus = this.plus.bind(this);
    }

    changeColorC() {
        this.setState({
            colorC: GetRandomColor()
        });
    }

    plus() {

    }

    render() {
        return (
            <div style={{height: 400, width: 400,backgroundColor: this.props.colorB, padding: 30}}>
                Component B
                <button onClick={this.changeColorC}>Button B</button><br />
                <button onClick={this.plus}>+</button>
                <ComponentC colorC={this.state.colorC} changeColorA={this.props.changeColorA}/>
            </div>
        );
    }
}

ComponentB.propTypes = {
    colorB: PropTypes.string,
    changeColorA: PropTypes.func
}

ComponentB.defaultProps = {
    colorB: "#000000",
    changeColorA: function () {
        console.log("please assgin function !");
    }
}

export default ComponentB;